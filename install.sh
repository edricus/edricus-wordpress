#!/bin/bash
if [[ "$EUID" -ne 0 ]]; then 
  echo "Please run as root"
  exit
fi
WORDPRESS_APACHE=/etc/apache2/sites-available/wordpress.conf
WORDPRESS_LOCATION='/var/www'
WORDPRESSDATA_LOCATION='/srv/wordpress/data'

dependencies_install() {
apt-get install -yqq php7.4 php7.4-mysql mariadb-server
}

wordpress_download() {
if [[ ! -d /var/www ]]; then
  mkdir /var/www
fi
wget https://wordpress.org/latest.tar.gz
tar -xzvf latest.tar.gz -C /var/www/
chmod -R 700 "${WORDPRESS_LOCATION}"/wordpress
mkdir -p "${WORDPRESSDATA_LOCATION}"
chown -R www-data:www-data "${WORDPRESSDATA_LOCATION}"
chown -R www-data:www-data "${WORDPRESS_LOCATION}"
chmod -R 700 "${WORDPRESSDATA_LOCATION}"
}

mariadb_configuration() {
VERIFY=1
while [[ ${WORDPRESSPASSWD} != ${VERIFY} ]]; do
  read -rp " -> create Wordpress user: " WORDPRESSUSER
  read -rsp " -> password for "${WORDPRESSUSER}": " WORDPRESSPASSWD
  printf "\n"
  read -rsp " -> verify password: " VERIFY
  printf "\n"
  if [[ ${WORDPRESSPASSWD} != ${VERIFY} ]]; then
    sleep 1
    echo "passwords doesn't match, retry"
  fi
done
mysql_secure_installation
mysql << MYSQL
create user "${WORDPRESSUSER}"@'localhost' identified by "${WORDPRESSPASSWD}";
create database wordpress;
GRANT ALL PRIVILEGES ON wordpress.* TO "${WORDPRESSUSER}"@'localhost';
FLUSH PRIVILEGES;
MYSQL
}

wordpress_install (){
WORDPRESS_CONFIG=/var/www/wordpress/wp-config.php
cat << EOF > ${WORDPRESS_CONFIG}
<?php
define( 'DB_NAME', 'wordpress' );
define( 'DB_USER', '${WORDPRESSUSER}' );
define( 'DB_PASSWORD', '${WORDPRESSPASSWD}' );
define( 'DB_HOST', 'localhost' );
define( 'DB_CHARSET', 'utf8' );
define( 'DB_COLLATE', '' );
define( 'WP_DEBUG', true );
define( 'FORCE_SSL_ADMIN', true );
#define( 'WP_HTTP_BLOCK_EXTERNAL', true );
#define( 'WP_ACCESSIBLE_HOSTS', 'api.wordpress.org,*.github.com' );
\$table_prefix = 'wp_';
if ( ! defined( 'ABSPATH' ) ) {
  define( 'ABSPATH', __DIR__ . '/' );
}
require_once ABSPATH . 'wp-settings.php';
EOF
}

apache_configuration() {
read -rp 'Name of the server: ' SERVERNAME
cat << EOF > ${WORDPRESS_APACHE}
alias '/wordpress' '${WORDPRESS_LOCATION}/wordpress'
<VirtualHost *:443>
  ServerName ${SERVERNAME}
  DocumentRoot ${WORDPRESS_LOCATION}/wordpress
  ErrorLog ${APACHE_LOG_DIR}/wordpress_error.log
  CustomLog ${APACHE_LOG_DIR}/wordpress_access.log combined
    <Directory ${WORDPRESS_LOCATION}/wordpress>
      Require all granted
      AllowOverride All
      Options FollowSymLinks MultiViews
    </Directory>
  SSLEngine on
  SSLCertificateFile /etc/apache2/ssl/wordpress.cert
  SSLCertificateKeyFile /etc/apache2/ssl/wordpress.key
</VirtualHost>
EOF
a2ensite wordpress
}

ssl_configuration() {
mkdir /etc/apache2/ssl
openssl req -new -newkey rsa:2048 -days 365 -nodes -x509 \
  -subj "/C=FR/ST=Ile-de-France/L=Paris/O=${SERVERNAME}/CN=${SERVERNAME}" \
  -keyout /etc/apache2/ssl/wordpress.key  -out /etc/apache2/ssl/wordpress.cert
a2enmod ssl
}

main(){
dependencies_install  
wordpress_download
mariadb_configuration
wordpress_install
apache_configuration
ssl_configuration
  systemctl restart apache2
  systemctl enable apache2
}

# EXEC
if [[ $1 == '--install' || $1 == '-i' ]]; then
  main
  echo 'Wordpress install done !'
  echo "Wordpress website location: ${WORDPRESS_LOCATION} with permissions www-data:700"
  echo "Wordpress data location: ${WORDPRESSDATA_LOCATION} with permissions www-data:700" 
  echo 'Your wordpress is accessible via http://localhost/wordpress'
  exit 0
fi
if [[ $1 == '-h' || -z $1 ]]; then
  echo 'Usage: ./wordpress.sh [OPTION]'
  echo '  -i, --install          install Wordpress (download wordpress, configure mariadb, apache2 and install redis)'
fi

